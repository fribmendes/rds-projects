package bank;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by frm on 15/02/16.
 */
public class BankImpl implements Bank {
    private HashMap<String, Integer> accounts;

    private static int count = 0;

    public BankImpl() {
        accounts = new HashMap<>();
    }

    @Override
    public int balance(String account) {
        // TODO: add account existence verification
        // if you want to get really arrogant about accounts existing, make it return a Maybe<Integer>
        return accounts.get(account);
    }

    public synchronized int movement(String account, int amount) {
        int balance = accounts.get(account);
        int newBalance = balance + amount;

        if(newBalance < 0)
            return -1;

        accounts.put(account, newBalance);
        return newBalance;
    }

    public synchronized String create(int initialAmount) {
        String name = Bank.PREFIX + ++count;
        accounts.put(name, initialAmount);
        return name;
    }

    public void connect() throws IOException {
        /* Not needed here */
    }
}
