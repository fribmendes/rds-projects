package client;

import bank.Bank;
import bank.BankStub;

import java.io.IOException;

/**
 * Created by frm on 15/02/16.
 */
public class Client {
    public static void main(String[] args) throws IOException {
        Bank b = new BankStub();
        b.connect();
        System.out.println("Just connected to the server");
        String a1 = b.create(200);
        System.out.println("Just created 1 account");
        String a2 = b.create(300);
        System.out.println("Just created 2 accounts");
        b.movement(a1, 200);
        b.movement(a2, -200);
        System.out.println("Account 1 has " + b.balance(a1));
        System.out.println("Account 2 has " + b.balance(a2));
        b.movement(a2, -1000);
        System.out.println("Account 2 has " + b.balance(a2));
    }
}
