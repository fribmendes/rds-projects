import net.sf.jgcs.*;
import net.sf.jgcs.ip.IpGroup;
import net.sf.jgcs.ip.IpProtocolFactory;
import net.sf.jgcs.ip.IpService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

/**
 * Created by frm on 22/02/16.
 */
public class Chat implements MessageListener {
    private ControlSession control;
    private DataSession data;
    private Service service;

    private BufferedReader in;

    public static final String DEFAULT_MULTICAST_ADDRESS = "231.20.20.20:55500";

    public Chat() throws UnknownHostException, GroupException {

        // We are using a multicast address, needs to start with 225
        GroupConfiguration gc = new IpGroup(DEFAULT_MULTICAST_ADDRESS);

        // Generating our IP protocol
        ProtocolFactory pf = new IpProtocolFactory();
        Protocol p = pf.createProtocol();

        control = p.openControlSession(gc);
        data = p.openDataSession(gc);
        service = new IpService();

    }

    public Object onMessage(Message m) {
        String s = new String(m.getPayload());
        String sender = m.getSenderAddress().toString();
        System.out.println(sender + ":" + s);
        return null;
    }

    public void start() throws IOException {
        while(true) {
            String s = in.readLine();
            Message m = data.createMessage();
            m.setPayload(s.getBytes());
            data.multicast(m, service, null);
        }
    }

    private Chat init() throws GroupException {
        // Register our callbacks
        data.setMessageListener(this);

        // join the group
        control.join();

        // open the reader for the system input
        in = new BufferedReader(new InputStreamReader(System.in));

        return this;
    }

    public static void main(String[] args) throws IOException {
        new Chat().init().start();
    }

}
