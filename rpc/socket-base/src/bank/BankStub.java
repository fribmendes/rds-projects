package bank;

import server.Server;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by frm on 15/02/16.
 */
public class BankStub implements Bank {
    private Socket s;
    private BufferedReader in;
    private PrintWriter out;
    private InetAddress address;
    private int port;

    public BankStub() throws IOException {
        port = Server.DEFAULT_PORT;
        address = InetAddress.getLocalHost();
    }

    public BankStub(InetAddress address, int port) {
        this.port = port;
        this.address = address;
    }

    @Override
    public int balance(String account) {
        write( marshall("balance", new String[] { account }) );
        try {
            return readInt();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int movement(String account, int amount) {
        write( marshall("movement", new String[] { account, Integer.toString(amount) }) );
        try {
            return readInt();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public String create(int initialAmount) {
        write( marshall("create", new String[] { Integer.toString(initialAmount) }) );
        try {
            return read();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void connect() throws IOException {
        s = new Socket(address, port);
        in = new BufferedReader(new InputStreamReader(s.getInputStream()));
        out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()));
    }

    private String marshall(String method, String[] args) {
        StringBuilder sb = new StringBuilder();
        sb.append(method);
        if(args != null) {
            sb.append(":");

            // comma separated args
            // don't add a comma to the last one
            for(int i = 0; i < args.length - 1; i++)
                sb.append(args[i]).append(",");

            // appending the last arg so it doesn't get a comma
            sb.append(args[args.length - 1]);
        }

        return sb.toString();
    }

    private void write(String s) {
        out.write(s + "\n"); // bring the hammer down
        out.flush();
    }

    private String read() throws IOException {
        return in.readLine();
    }

    private int readInt() throws IOException {
        return Integer.parseInt(read().trim());
    }
}
