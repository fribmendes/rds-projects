package bank;

import java.io.IOException;

/**
 * Created by frm on 15/02/16.
 */
public interface Bank {
    String PREFIX = "ACCOUNT_";

    int balance(String account);
    int movement(String account, int amount);
    String create(int initialAmount);
    void connect() throws IOException;
}
