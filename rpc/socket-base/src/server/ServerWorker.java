package server;

import bank.Bank;
import bank.BankImpl;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by frm on 15/02/16.
 */
public class ServerWorker implements Runnable, Bank {
    private BankImpl bank;
    private Socket c;
    private BufferedReader in;
    private PrintWriter out;

    public ServerWorker(Socket client, BankImpl bank) {
        c = client;
        this.bank = bank;
    }

    public void connect() throws IOException {
        in = new BufferedReader(new InputStreamReader(c.getInputStream()));
        out = new PrintWriter(new OutputStreamWriter(c.getOutputStream()));
    }

    @Override
    public void run() {
        try {
            connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true) {
            try {
                handle();
            } catch (IOException e) {
                // e.printStackTrace();
                System.out.println("Client disconnected");
                break;
            }
        }
    }

    private void handle() throws IOException {
        String[] args = unmarshall( read() );
        String reply;

        switch (args[0]) {
            case "movement":
                reply = Integer.toString( movement(args[1], Integer.parseInt(args[2])) );
                break;
            case "create":
                reply = create(Integer.parseInt(args[1]));
                break;
            case "balance":
                reply = String.valueOf( balance(args[1]) );
                break;
            default:
                reply = "Unknown method called";
        }

        write(reply);
    }

    @Override
    public int balance(String account) {
        return bank.balance(account);
    }

    @Override
    public int movement(String account, int amount) {
        return bank.movement(account, amount);
    }

    @Override
    public String create(int initialAmount) {
        return bank.create(initialAmount);
    }

    private String read() throws IOException {
        String s;
        try {
            s = in.readLine().trim();
        } catch(NullPointerException e) {
            throw new IOException(e);
        }

        return s;
    }

    private void write(String s) {
        out.write(s + "\n");
        out.flush();
    }

    private String[] unmarshall(String s) {
        final String[] cmd = s.split(":");

        if (! cmd[1].contains(","))
            return cmd;

        String[] args = (cmd[1]).split(",");
        String[] params = new String[args.length + 1];
        params[0] = cmd[0];
        System.arraycopy(args, 0, params, 1, args.length);
        return params;
    }
}
