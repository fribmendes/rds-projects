package server;

import bank.Bank;
import bank.BankImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;

/**
 * Created by frm on 15/02/16.
 */
public class Server {
    private int port;
    private BufferedReader in;
    private PrintWriter out;
    private ServerSocket socket;
    private BankImpl bank;


    public static final int DEFAULT_PORT = 3000;

    public Server() {
        this.port = DEFAULT_PORT;
        this.bank = new BankImpl();
    }

    public Server(int p) {
        this.port = p;
        this.bank = new BankImpl();
    }

    private void listen() throws IOException {
        socket = new ServerSocket(port);
    }

    private void run() throws IOException {
        listen();

        while(true) {
            System.out.println("Waiting for a new client");
            accept();
        }
    }

    private void accept() throws IOException {
        new Thread( new ServerWorker(socket.accept(), bank) ).start();
    }

    public static void main(String[] args) throws IOException {
        new Server().run();
    }
}
